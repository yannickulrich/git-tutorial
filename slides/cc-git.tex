%PDFLATEX
% vim: foldmethod=expr
% vim: foldexpr=getline(v\:lnum)=~#\'^%%%%%\'?'>1\'\:\'=\'
%%%%%%%%%%%%%%  Header                                  %%%%%%%%%%%%%%
\documentclass[usepdftitle=false]{beamer}
\usepackage[super]{nth}


\usepackage{tikz}
\usetikzlibrary{matrix}

\title{(Somewhat) advanced git practises}
\subtitle{IPPP Code Club}
\date[22.02.21]{\nth{22} Feburary 2021}

\author[Yannick Ulrich]{Yannick Ulrich}
\institute{IPPP, University of Durham}
\usetheme{ippp}
\renewcommand{\Cite}[1]{\nocolour{\color{skyblue}\scriptsize [#1]}}

\def\commits{}
\newcommand\commit[2]{
    \node[commit] (#1) {};
    \node[clabel] at (CN|-#1) {\phantom{\tt 1}};
    \global\expandafter\def\expandafter\commits\expandafter{\commits
        \node[clabel] at (CN|-#1) {\texttt{#1}: #2};
    }
}
\newcommand\ghost[1]{\coordinate (#1);}
\newcommand\connect[2]{\path (#1) to[out=90,in=-90] (#2);}
\tikzstyle{commit}=[draw,circle,fill=white,inner sep=0pt,minimum size=5pt]
\tikzstyle{clabel}=[right,outer sep=2em]
\tikzstyle{every path}=[draw]

\hypersetup{
   pdfauthor={Yannick Ulrich},
   pdftitle={Somewhat advanced git practises}}


%%%%%%%%%%%%%%  Body                                    %%%%%%%%%%%%%%
\begin{document}

\begin{frame}{}
\titlepage
Code \& slides: \small\url{https://gitlab.com/yannickulrich/git-tutorial}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%  Disclaimer                              %%%%%%%%%%%%%%
\begin{frame}{\small disclaimer}
\small
\begin{columns}[c]
\hspace{-1cm}\begin{column}{.60\textwidth}
\begin{itemize}
\setlength\itemsep{1em}
    \item
    I'm \emph{not} a git expert

    \item
    this isn't necessarily how git is supposed to be used

    \item
    just how I use it in {\sc McMule}

    \item
    this is going to be 100\% terminal. Other solutions exist
\end{itemize}
\end{column}%
\begin{column}{.4\textwidth}
\includegraphics[width=\linewidth]{xkcd.png}\\
\tiny\href{https://xkcd.com/1597/}{xkcd.com/1597}
\end{column}%
\end{columns}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%  Mini introduction                       %%%%%%%%%%%%%%
\begin{frame}[fragile]{\small mini introduction}
\small
\begin{itemize}
    \item
    git is a VCS using \emph{directed acyclic graph}\\
    (entries can have multiple parents \hence merging)

    \item
    snapshots are immutable and addressed by their SHA1 hash

    \item
    references point to snapshots

\end{itemize}
\begin{tikzpicture}

\node(CN) at (0,0) {};
\matrix [column sep={1em,between origins},row sep=\lineskip]
{
\commit{02fbd81}{HEAD \hence master} & &\\
\ghost{b0a} & \ghost{b1a} & \\
&&\commit{65e72fe}{feature-2} \\
\commit{a399e68}{}  &&\ghost{branch2} \\
\ghost{branch0} & \commit{54ba4b2}{origin/feature} &\\
\commit{c589395}{origin/master} && \\
\commit{d764b48}{} & \ghost{branch1}& \\
\commit{b3bd158}{v0.1} && \\
\commit{86f2990}{} && \\
};
\commits

\connect{86f2990}{b3bd158};
\connect{b3bd158}{d764b48};
\connect{b3bd158}{branch1};
\connect{d764b48}{c589395};
\connect{branch1}{54ba4b2};
\connect{c589395}{branch0};
\connect{branch0}{branch2};
\connect{branch0}{a399e68};
\connect{branch2}{65e72fe};
\connect{a399e68}{b0a};
\connect{54ba4b2}{b1a};
\connect{b0a}{02fbd81};
\connect{b1a}{02fbd81};

\end{tikzpicture}


\end{frame}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%  Simple usage                            %%%%%%%%%%%%%%
\begin{frame}{\small using git: the simple bit}
\small
\begin{enumerate}
    \item
    fetch from server: {\tt git fetch \&\& git merge}

    \item
    see what changed: {\tt git status}, {\tt git diff}

    \item
    before commit: move into staging area: {\tt git add -p}

    \item
    commit: {\tt git commit -m "<msg>"}

    \item
    (repeat if necessary)

    \item
    sync with server: {\tt git push}

\end{enumerate}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%  Best practise                           %%%%%%%%%%%%%%
\begin{frame}{\small suggested practises}
\small
\begin{itemize}

    \item
    keep commits atomic, i.e. only one thing per commit

    \item
    keep diffs clean and avoid large blobs

    \item
    keep commit messages short and, if applicable, refer to issues,
    commits, etc.

    \item
    don't push until reasonably sure code is correct\\
    (builds, passes tests, etc)

    \item
    don't rewrite pushed history on {\tt origin} / force push

    \item
    ... unless you know \emph{exactly} what you do and nobody pulled
    it

    \item
    multiple processes/scenarios/models?\\
    \Hence multiple branches ({\tt git checkout -b <branch name>})

    \item
    use a master branch for global things

\end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%  Things I'll cover                       %%%%%%%%%%%%%%
\begin{frame}{\small advanced things to do}
\small
\head{things I'll cover}: all things I've used often for {\sc McMule}
\begin{itemize}
    \item
    simple fast-forward merge

    \item
    cherry picking

    \item
    merge conflicts

    \item
    fixing local mistakes ({\tt reset} and {\tt rebase})

    \item
    octopus merging (if time left)

\end{itemize}

\head{things I won't cover}
\begin{itemize}
    \item
    {\tt git bisect} -- really cool scriptable method to find bugs efficiently

    \item
    {\tt git revert}, {\tt git push -f} -- public history changes

    \item
    {\tt git stash} -- quick cleaning of work directory

    \item
    submodules, apply, ...

\end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%  The example                             %%%%%%%%%%%%%%
\begin{frame}{\small the example}
\small
\begin{itemize}
    \item
    mini project with code stolen from {\sc McMule}

    \item
    three people: Alice, Bob, Marc (the maintainer)

    \item
    Alice and Bob each have their own projects and associated
    branches

    \item
    we have to fix their git issues

\end{itemize}

{\tt \$ ./build-project.sh}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%  Further reading                         %%%%%%%%%%%%%%
\begin{frame}{\small further reading}
\small
\begin{itemize}
    \item
    suggestions on how to write commit messages
    \url{https://chris.beams.io/posts/git-commit/}

    \item
    a lecture going into detail how git works under the hood
    \url{https://missing.csail.mit.edu/2020/version-control/}

    \item
    interactive game-like tutorial
    \url{https://learngitbranching.js.org/}

\end{itemize}
\end{frame}

\end{document}
