# git tutorial

This is the example code for mini-talk I gave at IPPP Code Club on 22 February
2021. You can find the slides [here](https://gitlab.com/yannickulrich/git-tutorial/-/jobs/artifacts/master/file/slides/cc-git.pdf?job=buildslides).


## what is covered
This example is covering some of the more complicated features in git.
Currently included is

 * simple fast-forward merge
 * cherry picking
 * merge conflicts
 * fixing local mistakes (`reset` and `rebase`)
 * octopus merging

I might cover some of the following later if I return to this

 * `git bisect`: really cool scriptable method to find bugs efficiently
 * `git revert`, `git push -f`: public history changes
 * `git stash`: quick storage and clean of work directory
 * submodules, apply, ...


## This project
This is a mini project with code stolen from
[McMule](https://gitlab.com/mule-tools/mcmule). It has three people working on
it. Two students Alice and Bob and Marc, the project's maintainer. Alga and
Bob each work on their own projects with a branch each (Alice on $`\mu\to
e\nu\bar\nu`$ and Bob on $`ee\to ee`$). We have to fix the issue they encounter
during their work.

To start this, just run ``./build-project.sh`. It will give you the problems
and a shell to solve them. You can investigate what has happened by checking
`git log` (see also the snippets).

Alternatively, you can obtain a tar file with repositories for all
tasks [here](https://gitlab.com/yannickulrich/git-tutorial/-/jobs/artifacts/master/file/remote.tar.gz?job=buildrepo).
```
# Get Task 1
$ git clone remote/01-merge task1
$ cd task1
# solve Task 1
```

### Task 1: merging `master` into `mudec`
At this point, the project graph looks like this
```
$ git lg1
* fd90a47 - (2 days ago) Added DiLog function - Marc (HEAD -> master, origin/master)
| * a4ba8c8 - (3 days ago) Added Moller user file - Bob (origin/moller, moller)
| * a97cd00 - (3 days ago) Added integrand for Moller - Bob
| * e7e029b - (3 days ago) Added LO matrix elements for Moller scattering - Bob
|/  
| * fde76a3 - (3 days ago) Follow-up 31fb174: Added GF - Alice (origin/mudec, mudec)
| * 825899e - (3 days ago) Cleaned user file - Alice
| * a173912 - (3 days ago) Finished integrand - Alice
| * 019f9f7 - (3 days ago) Follow-up b3b1dcb: Fixed name of integrand - Alice
| * b3b1dcb - (3 days ago) Copied integrand for mudec - Alice
| * 8d5af62 - (3 days ago) Added decay phase space - Alice
| * 31fb174 - (3 days ago) Added legacy matrix elements - Alice
|/  
* 2a3c1c2 - (4 days ago) Follow-up 14d0200: Fixed ndim - Marc
* 662e66b - (4 days ago) Follow-up a2e3460: Fixed makefile - Marc
* a2e3460 - (4 days ago) Added user file and read s from user - Marc
* b4d75c9 - (4 days ago) Added cleaning target to makefile - Marc
* 30addca - (4 days ago) Fixed build system - Marc
* fec340e - (4 days ago) [ci skip] Added integrand - Marc
* e6c8f11 - (4 days ago) Added phase space routine - Marc
* 14d0200 - (4 days ago) Added empty integrand - Marc
* 4758ec3 - (4 days ago) Added LO matrix element - Marc
* 85c5d31 - (4 days ago) Fixed vegas - Marc
* 0a1eb3c - (4 days ago) Added main script - Marc
* f949838 - (4 days ago) Added makefile - Marc
* 24c999d - (4 days ago) Added some legacy code - Marc
* 3cd7873 - (4 days ago) Added .gitignore - Marc
```
Marc just added the `DiLog` function (`fd90a47`) which Alice needs. Hence, the
first thing we need to do is to merge the  `master` branch into the `mudec`
branch. To do this, we first update all relevant branches
```
$ git fetch && git merge # Pull is discouraged
$ git checkout master
Already on 'master'
Your branch is up-to-date with 'origin/master'.
$ git checkout mudec
Switched to branch 'mudec'
Your branch is up-to-date with 'origin/mudec'.
```
We can now execute the merge
```
$ git merge master
Auto-merging functions.f95
Merge made by the 'recursive' strategy.
 functions.f95 | 108 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 1 file changed, 108 insertions(+)
```


### Task 2: Cherry picking
After some time, Bob has added the function `branch_cut` that should have been
added to the `master` branch. However, we cannot undo it (maybe because he
already pushed it), so we need to cherry-pick the commit. We find the relevant
commit
```
$ git lg1
*   90b84db - (2 minutes ago) Merge branch 'master' into mudec - Yannick Ulrich (origin/mudec, mudec)
|\  
| * fd90a47 - (2 days ago) Added DiLog function - Marc (HEAD -> master, origin/master)
* | fde76a3 - (3 days ago) Follow-up 31fb174: Added GF - Alice
* | 825899e - (3 days ago) Cleaned user file - Alice
* | a173912 - (3 days ago) Finished integrand - Alice
* | 019f9f7 - (3 days ago) Follow-up b3b1dcb: Fixed name of integrand - Alice
* | b3b1dcb - (3 days ago) Copied integrand for mudec - Alice
* | 8d5af62 - (3 days ago) Added decay phase space - Alice
* | 31fb174 - (3 days ago) Added legacy matrix elements - Alice
|/  
| * 567502b - (2 days ago) Added LO matrix element for Bhabha scattering - Bob (origin/moller, moller)
| * a476ead - (2 days ago) Added function for Re[Log[-x]^2] - Bob
| * a4ba8c8 - (3 days ago) Added Moller user file - Bob
| * a97cd00 - (3 days ago) Added integrand for Moller - Bob
| * e7e029b - (3 days ago) Added LO matrix elements for Moller scattering - Bob
|/  
* 2a3c1c2 - (4 days ago) Follow-up 14d0200: Fixed ndim - Marc
* 662e66b - (4 days ago) Follow-up a2e3460: Fixed makefile - Marc
* a2e3460 - (4 days ago) Added user file and read s from user - Marc
* b4d75c9 - (4 days ago) Added cleaning target to makefile - Marc
* 30addca - (4 days ago) Fixed build system - Marc
* fec340e - (4 days ago) [ci skip] Added integrand - Marc
* e6c8f11 - (4 days ago) Added phase space routine - Marc
* 14d0200 - (4 days ago) Added empty integrand - Marc
* 4758ec3 - (4 days ago) Added LO matrix element - Marc
* 85c5d31 - (4 days ago) Fixed vegas - Marc
* 0a1eb3c - (4 days ago) Added main script - Marc
* f949838 - (4 days ago) Added makefile - Marc
* 24c999d - (4 days ago) Added some legacy code - Marc
* 3cd7873 - (4 days ago) Added .gitignore - Marc
```
The relevant commit is `a476ead`. We can now use
```
$ git cherry-pick -x a476ead
[master 480f880] Added function for Re[Log[-x]^2]
 Author: Bob <bob@dur.ac.uk>
 Date: Wed Feb 17 04:09:27 2021 +0000
 1 file changed, 15 insertions(+)
```

### Task 3: merging `mudec` and `moller` into `master`
We now want to merge the entire moller and mudec branches into the master
branch. This is more complicated. We first check out every branch involved
```
$ git checkout mudec
Switched to branch 'mudec'
Your branch is up-to-date with 'origin/mudec'.
$ git checkout moller
Switched to branch 'moller'
Your branch is up-to-date with 'origin/moller'.
$ git checkout master
Switched to branch 'master'
Your branch is up-to-date with 'origin/master'.
```
and begin by merging `mudec` into `master`
```
$ git merge mudec
Auto-merging functions.f95
Merge made by the 'recursive' strategy.
 functions.f95 | 63 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 main.f95      | 24 ++++++++++++++++++++++++
 mat_el.f95    | 50 ++++++++++++++++++++++++++++++++++++++++++++++++++
 user.f95      |  3 ---
 4 files changed, 137 insertions(+), 3 deletions(-)
 ```
 The merge of `moller` is more complicated.
 ```
 $ git merge moller
Auto-merging user.f95
CONFLICT (content): Merge conflict in user.f95
Auto-merging mat_el.f95
CONFLICT (content): Merge conflict in mat_el.f95
Auto-merging main.f95
CONFLICT (content): Merge conflict in main.f95
Auto-merging functions.f95
CONFLICT (content): Merge conflict in functions.f95
Automatic merge failed; fix conflicts and then commit the result.
```
We now need to check where the problems lie
```
$ git status
On branch master
Your branch is ahead of 'origin/master' by 9 commits.
  (use "git push" to publish your local commits)

You have unmerged paths.
  (fix conflicts and run "git commit")
  (use "git merge --abort" to abort the merge)

Unmerged paths:
  (use "git add <file>..." to mark resolution)

	both modified:   functions.f95
	both modified:   main.f95
	both modified:   mat_el.f95
	both modified:   user.f95

no changes added to commit (use "git add" and/or "git commit -a")
```
We now need to look at the files `functions.f95`, `main.f95`, `mat_el.f95`,
`user.f95` and fix all occuring mistakes
```
$ vi functions.f95 main.f95 user.f95 mat_el.f95
```
Merge conflicts are indicated by
```
code that works
<<<<<<< HEAD
code from the current HEAD, i.e. master
=======
code from the branch we are trying to merge
>>>>>>> moller
code that works
```
This is relatively straightforward in this case. Once everything is
done, run
```

$ git add functions.f95 main.f95 user.f95 mat_el.f95
$ git commit
[master 97202bc] Merge branch 'moller'
```


### Task 4: fixing mistakes
We have just noticed that five commits ago we made a mistake We didn't
declare some variables that are needed. Of course we could just do
that now and take the hit -- or we try to fix our mistake. We could
just go back one commit at a time with `git reset --soft HEAD~1`, fix
the problem and re-commit However, we'd have to redo quite a bit of
work. Instead, we can use `git rebase -i`.

The relevant commits are
```
$ git slog dcce1f9^..9e8f05e 
* commit: 9e8f05e <9e8f05e1b8fa29334c4ef2ade5c2c4f98155d7e8>  (HEAD -> mudec, origin/mudec) 
| date: Thu Feb 18 00:20:23 2021 +0000 2 days ago
| author: Alice <alice@dur.ac.uk>
| Added integrands
| 
* commit: 95877a7 <95877a7a6f8e5959d73d2a4dd7c4382eb23bcfe3>
| date: Wed Feb 17 23:20:23 2021 +0000 2 days ago
| author: Alice <alice@dur.ac.uk>
| Added PSD3 phase space
| 
* commit: d4d9b0c <d4d9b0c13821236e68c372cc81d4389c75098509>
| date: Wed Feb 17 22:20:23 2021 +0000 2 days ago
| author: Alice <alice@dur.ac.uk>
| Added LFV decays
| 
* commit: dcce1f9 <dcce1f958a8455cf40e45fdc6b41639af3d319bf>
| date: Fri Feb 19 14:20:22 2021 +0000 7 minutes ago
| author: Yannick Ulrich <yannick.ulrich@durham.ac.uk>
| Merge branch 'master' into mudec
| 
* commit: ae1e5f7 <ae1e5f78e48bf99fbbc97c5a84c02fce6c4a79c7>
  date: Wed Feb 17 07:20:22 2021 +0000 2 days ago
  author: Marc <marc@dur.ac.uk>
  Added DiLog function

```
We want to rename the commit `d4d9b0c` and add a commit fixing our
mistakes directly after it. We run
```
$ git rebase -i dcce1f9
```
and change the file accordingly to
```
edit d4d9b0c Added LFV decays
pick 95877a7 Added PSD3 phase space
pick 9e8f05e Added integrands
```
We can now rename the commit (which will change its hash)
```
$ git commit --amend -m "[ci skip] Added LFV decays"
[detached HEAD 34d8063] [ci skip] Added LFV decays
 Author: Alice <alice@dur.ac.uk>
 Date: Wed Feb 17 22:20:23 2021 +0000
 1 file changed, 24 insertions(+)
```
fix our mistake
```
$ git apply <<EOF
--- a/functions.f95
+++ b/functions.f95
@@ -7,6 +7,7 @@
   real (kind=prec), parameter :: pi = 3.14159265358979323846_prec
   real (kind=prec), parameter :: alpha = 1/137.035999084_prec
   real (kind=prec), parameter :: GF = 1._prec
+  real (kind=prec) :: CL, CR, mJ, pol1(4)
 
   real (kind=prec), parameter :: Mmu = 105.658375_prec     ! MeV
   real (kind=prec), parameter :: Mel = 0.510998950_prec    ! MeV
EOF
```
and stage and commit it
```
$ git add functions.f95
$ git commit -m "Added couplings"
[detached HEAD 8d54209] Added couplings
 1 file changed, 1 insertion(+)
```
Now we can continue the rebase
```
$ git rebase --continue 
Successfully rebased and updated refs/heads/mudec.
```


### Task 5: octopus merging

The `mat_el` file starts to grow quite a bit. Let us split it into
files for the three processes mue, mudec, and moller. However, I'd
like to keep the history (`git blame`) so we can't just chop it in
half. Instead, we need to do an octopus merge.

We will create a branch for each of the splits we do, rename the file
and delete the things we don't want.

We begin by splitting the `mudec` part
```
$ git checkout -b dummy-mudec
Switched to a new branch 'dummy-mudec'
$ git mv mat_el.f95 mudec-mat_el.f95
$ git commit -m "Renamed mat_el.f95"
[dummy-mudec c8e249b] Renamed mat_el.f95
 1 file changed, 0 insertions(+), 0 deletions(-)
 rename mat_el.f95 => mudec-mat_el.f95 (100%)
```
Next, we delete the superflous parts of `mudec-mat_el.f95` (line 8-86)
```
$ sed -i -e '8,68d' mudec-mat_el.f95
```
commit and go back to the `master` branch
```
$ git add mudec-mat_el.f95
$ git commit -m "Removed non-mudec code"
[dummy-mudec 2d25075] Removed non-mudec code
 1 file changed, 61 deletions(-)
$ git checkout master
Switched to branch 'master'
Your branch is up-to-date with 'origin/master'.
```
We now repeat this for the `mue` part where we delete lines 24-118
```
$ git checkout -b dummy-mue
Switched to a new branch 'dummy-mue'
$ git mv mat_el.f95 mue-mat_el.f95
$ git commit -m "Renamed mat_el.f95"
[dummy-mue 6ee6049] Renamed mat_el.f95
 1 file changed, 0 insertions(+), 0 deletions(-)
 rename mat_el.f95 => mue-mat_el.f95 (100%)
$ sed -i -e '24,118d' mue-mat_el.f95
$ git add mue-mat_el.f95
$ git commit -m "Removed non-mue code"
[dummy-mue 707a53f] Removed non-mue code
 1 file changed, 27 insertions(+), 122 deletions(-)
 rewrite mue-mat_el.f95 (76%)
$ git checkout master
Switched to branch 'master'
Your branch is up-to-date with 'origin/master'.
```
The last rename we can do on the `master` branch where we delete lines
8-23 and 69-118
```
$ git mv mat_el.f95 ee-mat_el.f95
$ git commit -m "Renamed mat_el.f95"
[master 4b8945f] Renamed mat_el.f95
 1 file changed, 0 insertions(+), 0 deletions(-)
 rename mat_el.f95 => ee-mat_el.f95 (100%)
$ sed -i -e '8,23d;69,118d' ee-mat_el.f95
$ git add ee-mat_el.f95
$ git commit -m "Removed non-ee code"
[master b22c11f] Removed non-ee code
 1 file changed, 66 deletions(-)
```
Now everything is prepared and the log looks as follow
```
git lg1
* b22c11f - (33 seconds ago) Removed non-ee code - Yannick Ulrich (HEAD -> master)
* 4b8945f - (34 seconds ago) Renamed mat_el.f95 - Yannick Ulrich
| * 707a53f - (76 seconds ago) Removed non-mue code - Yannick Ulrich (dummy-mue)
| * 6ee6049 - (76 seconds ago) Renamed mat_el.f95 - Yannick Ulrich
|/  
| * 2d25075 - (4 minutes ago) Removed non-mudec code - Yannick Ulrich (dummy-mudec)
| * c8e249b - (4 minutes ago) Renamed mat_el.f95 - Yannick Ulrich
|/  
*   2fd725e - (6 minutes ago) Merge branch 'moller' - Yannick Ulrich (origin/master)
```
We now need to merge all three branches together
```
$ git merge dummy-mue dummy-mudec --no-edit
Trying simple merge with dummy-mue
Trying simple merge with dummy-mudec
Merge made by the 'octopus' strategy.
 mudec-mat_el.f95 | 61 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 mue-mat_el.f95   | 27 +++++++++++++++++++++++++++
 2 files changed, 88 insertions(+)
 create mode 100644 mudec-mat_el.f95
 create mode 100644 mue-mat_el.f95
```
We now have three files, each with their own content without breaking
the history
```
$ git blame mudec-mat_el.f95
56f8c485 mat_el.f95 (Marc  2021-02-15 16:33:48 +0000  1)                  !!!!!!!!!!!!!!!!!!!!!!!!!
56f8c485 mat_el.f95 (Marc  2021-02-15 16:33:48 +0000  2)                        MODULE MAT_EL
56f8c485 mat_el.f95 (Marc  2021-02-15 16:33:48 +0000  3)                  !!!!!!!!!!!!!!!!!!!!!!!!!
56f8c485 mat_el.f95 (Marc  2021-02-15 16:33:48 +0000  4)   use functions
56f8c485 mat_el.f95 (Marc  2021-02-15 16:33:48 +0000  5) 
56f8c485 mat_el.f95 (Marc  2021-02-15 16:33:48 +0000  6) contains
56f8c485 mat_el.f95 (Marc  2021-02-15 16:33:48 +0000  7) 
56f8c485 mat_el.f95 (Marc  2021-02-15 16:33:48 +0000  8) 
0a6387b7 mat_el.f95 (Alice 2021-02-16 16:33:49 +0000  9)   FUNCTION M2ENN(q1,q2,q3,q4)
0a6387b7 mat_el.f95 (Alice 2021-02-16 16:33:49 +0000 10) 
0a6387b7 mat_el.f95 (Alice 2021-02-16 16:33:49 +0000 11)   real (kind=prec) :: q1(4),q2(4),q3(4),q4(4)
0a6387b7 mat_el.f95 (Alice 2021-02-16 16:33:49 +0000 12)   real (kind=prec) :: s12,s13,s14,s23,s24,s34
0a6387b7 mat_el.f95 (Alice 2021-02-16 16:33:49 +0000 13)   real (kind=prec) :: m2enn
...
```
with the log
```
$ git lg1
*-.   46ad4dc - (63 seconds ago) Merge branches 'dummy-mue' and 'dummy-mudec' - Yannick Ulrich (HEAD -> master)
|\ \  
| | * 2d25075 - (6 minutes ago) Removed non-mudec code - Yannick Ulrich (dummy-mudec)
| | * c8e249b - (6 minutes ago) Renamed mat_el.f95 - Yannick Ulrich
| * | 707a53f - (3 minutes ago) Removed non-mue code - Yannick Ulrich (dummy-mue)
| * | 6ee6049 - (3 minutes ago) Renamed mat_el.f95 - Yannick Ulrich
| |/  
* | b22c11f - (3 minutes ago) Removed non-ee code - Yannick Ulrich
* | 4b8945f - (3 minutes ago) Renamed mat_el.f95 - Yannick Ulrich
|/  
*   2fd725e - (8 minutes ago) Merge branch 'moller' - Yannick Ulrich (origin/master)
```



## Further reading

 * suggestions on how to write commit messages https://chris.beams.io/posts/git-commit/
 * a lecture going into detail how git works under the hood https://missing.csail.mit.edu/2020/version-control/
 * interactive game-like tutorial https://learngitbranching.js.org/
 * octopus merging https://devblogs.microsoft.com/oldnewthing/20190916-00/?p=102892
