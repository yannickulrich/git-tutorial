#!/bin/bash
set -e

function pushsave {
    git push --set-upstream origin master || true
    git push --set-upstream origin mudec  || true
    git push --set-upstream origin moller || true
    cp -R ../remote/main ../remote/$1
}

function gc {
    git add ${@:4}
    case $1 in
        A*)
            name="Alice"
            email="alice@dur.ac.uk"
            ;;
        B*)
            name="Bob"
            email="bob@dur.ac.uk"
            ;;
        M*)
            name="Marc"
            email="marc@dur.ac.uk"
            ;;
    esac

    dat=`date --rfc-3339=seconds -d "$2 hour ago"`
    GIT_AUTHOR_DATE="$dat" \
    GIT_COMMITTER_DATE="$dat" \
    GIT_COMMITTER_NAME="$name" \
    GIT_COMMITTER_EMAIL="$email" \
    GIT_AUTHOR_NAME="$name" \
    GIT_AUTHOR_EMAIL="$email" \
    git commit -m "$3"
}

function getfile {
    branch=`git rev-parse --abbrev-ref HEAD`
    if [[ "$branch" == "master" ]]; then
        branch=""
    else
        branch="-$branch"
    fi
    cd ../
    for i in ${@:2}
    do
        if [[ $i == *"."* ]] ; then
            extension="${i##*.}"
            filename="${i%.*}"
            name=$filename$branch.$extension
        else
            name=$i$branch
        fi
        git show $1:files/$name > project/$i
    done
    cd project/
}


# make project directory
rm -rf remote/
mkdir -p remote/main
cd remote/main && git init --bare && cd -
rm -rf project/
mkdir project/
cd project


git init
git remote add origin ../remote/main
cp ../.gitignore .
gc M 100 "Added .gitignore" .gitignore

getfile fcfdb16 vegas_m.f95 functions.f95
gc M 99 "Added some legacy code" vegas_m.f95 functions.f95

getfile f58dab5 makefile
make functions.o vegas_m.o
gc M 98 "Added makefile" makefile

getfile a5ed77c makefile main.f95
make main || true
gc M 97 "Added main script" makefile main.f95

getfile a5ed77c vegas_m.f95
make main
gc M 96 "Fixed vegas" vegas_m.f95

getfile 4d69292 mat_el.f95 makefile
make main
gc M 94 "Added LO matrix element" mat_el.f95 makefile

getfile 2400695 main.f95
make main
gc M 94 "Added empty integrand" main.f95

getfile 32d9ccc8 functions.f95
make main
gc M 93 "Added phase space routine" functions.f95

getfile 32d9ccc main.f95
make main
gc M 93 "[ci skip] Added integrand" main.f95

getfile 32d9ccc makefile
gc M 92 "Fixed build system" makefile

getfile 4267121 makefile
make clean && make
gc M 92 "Added cleaning target to makefile" makefile

getfile 52ac445 user.f95 main.f95
make main || true
gc M 91 "Added user file and read s from user" user.f95 main.f95

getfile 52ac445 makefile
make main
recent=`git log --pretty=format:%h | head -n1`
gc M 91 "Follow-up $recent: Fixed makefile" makefile

getfile 52a228b main.f95
make main
echo -e "100\n10\n100\n5\n1231\n1000000\nem2em0" | ./main
recent=`git log --pretty=format:%h | head -n7 | tail -n1`
gc M 90 "Follow-up $recent: Fixed ndim" main.f95



# mudec branch
git checkout -b mudec
getfile 0c81a73 mat_el.f95
gc A 70 "Added legacy matrix elements" mat_el.f95

getfile e19f8c6 functions.f95 main.f95
gc A 69 "Added decay phase space" functions.f95

make || true
gc A 68 "Copied integrand for mudec" main.f95

getfile e3bb574 main.f95
make
recent=`git log --pretty=format:%h | head -n1`
gc A 68 "Follow-up $recent: Fixed name of integrand" main.f95

getfile c09863a main.f95 user.f95
make
gc A 67 "Finished integrand" main.f95
gc A 67 "Cleaned user file" user.f95

getfile e3946d2 functions.f95
make clean && make
echo -e "100\n10\n100\n5\n1231\n1000000\nm2enn0" | ./main
recent=`git log --pretty=format:%h | head -n6 | tail -n1`
gc A 60 "Follow-up $recent: Added GF" functions.f95


# moller branch
git checkout master
git checkout -b moller

getfile 64918dd mat_el.f95
make clean && make
gc B 62 "Added LO matrix elements for Moller scattering" mat_el.f95

getfile c52a17c main.f95
make
gc B 61 "Added integrand for Moller" main.f95

getfile 4d066dd user.f95 main.f95
make
echo -e "100\n10\n100\n5\n1231\n1000000\nee2ee0" | ./main
gc B 60 "Added Moller user file" user.f95 main.f95


# master branch
git checkout master
getfile 141fb64 functions.f95
make clean && make

gc M 55 "Added DiLog function" functions.f95


pushsave 01-merge

if [[ "$INTERACTIVE" == "0" ]]; then
    git checkout mudec
    git merge master --no-edit
else
    echo "-------------------------------------------------------------"
    echo " We now want to merge our changes into the mudec branch      "
    echo " where Alice needs them                                      "
    echo "-------------------------------------------------------------"
    bash
fi



# moller branch
git checkout moller
getfile 8a8e4d5 functions.f95
make clean && make

gc B 58 "Added function for Re[Log[-x]^2]" functions.f95

getfile 660c61b mat_el.f95
make
gc B 57 "Added LO matrix element for Bhabha scattering" mat_el.f95

# master branch
git checkout master

pushsave 02-cherry
if [[ "$INTERACTIVE" == "0" ]]; then
    recent=`git log --pretty=format:%h moller | head -n2 | tail -n1`
    git cherry-pick -x $recent
else
    echo "-------------------------------------------------------------"
    echo " Bob has added the function branch_cut that should have been "
    echo " added here. However, we cannot undo it (maybe because he    "
    echo " already pushed it), so we need to cherry-pick the commit    "
    echo "-------------------------------------------------------------"
    bash
fi
make clean && make


# master branch
pushsave 03-merge-conflict
if [[ "$INTERACTIVE" == "0" ]]; then
    git merge mudec --no-edit
    git merge moller || true
    getfile 876b745 functions.f95 main.f95 mat_el.f95 user.f95
    git add functions.f95 main.f95 mat_el.f95 user.f95
    git commit --no-edit
else
    echo "-------------------------------------------------------------"
    echo " We now want to merge the entire moller and mudec branches   "
    echo " into the master branch. This is more complicated            "
    echo "-------------------------------------------------------------"
    bash
fi

make clean && make


# mudec branch
git checkout mudec
getfile 573f53c mat_el.f95 functions.f95
make
gc A 40 "Added LFV decays" mat_el.f95
gc A 39 "Added PSD3 phase space" functions.f95

getfile c5458069 main.f95
make || true
gc A 38 "Added integrands" main.f95

pushsave 04-rebase
if [[ "$INTERACTIVE" == "0" ]]; then
    recent=`git log --pretty=format:%h | head -n4|tail -n1`
    git log --reverse --pretty=format:"pick %h %s" $recent..HEAD \
    | sed "1s/pick/edit/g" > rebase.txt
    GIT_SEQUENCE_EDITOR='cat rebase.txt >' git rebase -i $recent

    git apply <<EOF
--- a/functions.f95
+++ b/functions.f95
@@ -7,6 +7,7 @@
   real (kind=prec), parameter :: pi = 3.14159265358979323846_prec
   real (kind=prec), parameter :: alpha = 1/137.035999084_prec
   real (kind=prec), parameter :: GF = 1._prec
+  real (kind=prec) :: CL, CR, mJ, pol1(4)
 
   real (kind=prec), parameter :: Mmu = 105.658375_prec     ! MeV
   real (kind=prec), parameter :: Mel = 0.510998950_prec    ! MeV
EOF
    git commit --amend -m "[ci skip] Added LFV decays"
    git add functions.f95
    git commit -m "Added couplings"
    git rebase --continue
    rm rebase.txt
    git push --force
else
    echo "-------------------------------------------------------------"
    echo " We have just noticed that five commits ago we made a misake "
    echo " We didn't declare some variables that are needed. Of course "
    echo " we could just do that now and take the hit -- or we try to  "
    echo " fix our mistake. We could just go back one commit at a time "
    echo " with git reset --soft HEAD~1, fix the problem and re-commit "
    echo " However, we'd have to redo quite a bit of work. Instead, we "
    echo " can use git rebase -i                                       "
    echo "-------------------------------------------------------------"
    bash
fi
make


# master branch
git checkout master
pushsave 05-octopus
if [[ "$INTERACTIVE" == "0" ]]; then
    git checkout -b dummy-mudec
    git mv mat_el.f95 mudec-mat_el.f95
    git commit -m "Renamed mat_el.f95"
    sed -i -e '8,68d' mudec-mat_el.f95
    git add mudec-mat_el.f95
    git commit -m "Removed non-mudec code"
    git checkout master

    git checkout -b dummy-mue
    git mv mat_el.f95 mue-mat_el.f95
    git commit -m "Renamed mat_el.f95"
    sed -i -e '24,118d' mue-mat_el.f95
    git add mue-mat_el.f95
    git commit -m "Removed non-mue code"
    git checkout master

    git mv mat_el.f95 ee-mat_el.f95
    git commit -m "Renamed mat_el.f95"
    sed -i -e '8,23d;69,118d' ee-mat_el.f95
    git add ee-mat_el.f95
    git commit -m "Removed non-ee code"

    git merge dummy-mue dummy-mudec --no-edit
else
    echo "-------------------------------------------------------------"
    echo " The mat_el file starts to grow quite a bit. Let us split it "
    echo " into files for the three processes mue, mudec, and moller.  "
    echo " However, I'd like to keep the history (git blame) so we     "
    echo " can't just chop it in half. Instead, we need to do an       "
    echo " octopus merge.                                              "
    echo "-------------------------------------------------------------"
    bash
fi


cd ..
rm -rf remote/main
tar czvf remote.tar.gz remote/
