                          !!!!!!!!!!!!!!!!!!!!!!
                              MODULE FUNCTIONS
                          !!!!!!!!!!!!!!!!!!!!!!

  implicit none
  integer, parameter :: prec = selected_real_kind(15,32)
  real (kind=prec), parameter :: pi = 3.14159265358979323846_prec
  real (kind=prec), parameter :: alpha = 1/137.035999084_prec
  real (kind=prec), parameter :: Mmu = 105.658375_prec     ! MeV
  real (kind=prec), parameter :: Mel = 0.510998950_prec    ! MeV
  real (kind=prec) :: scms

contains
  FUNCTION BRANCHCUT(x,y,in,pre)
  real(kind=prec) :: x,y,in,branchcut,prefactor
  real(kind=prec),optional :: pre

  prefactor = 1.0

  if(present(pre)) prefactor = pre

  if(x > y) then
    branchcut = in**2 - prefactor*Pi**2
  else
    branchcut = in**2
  end if

  END FUNCTION

  FUNCTION S(q1,q2)
     ! S(q1,q2) =  2 q1.q2

  real (kind=prec), intent(in) :: q1(4),q2(4)
  real (kind=prec) :: S,dot_dot
  dot_dot = q1(4)*q2(4) - q1(1)*q2(1) - q1(2)*q2(2) - q1(3)*q2(3)
  S =  2*dot_dot
  END FUNCTION S


  FUNCTION SQ(q1)
     ! SQ(q1) = (q1)^2

  real (kind=prec), intent(in) :: q1(4)
  real (kind=prec) :: SQ

  SQ  = q1(4)**2 - q1(1)**2 - q1(2)**2 - q1(3)**2

  END FUNCTION SQ


  FUNCTION SQ_LAMBDA(ss,m1,m2) !! square root of \lambda(s,m1,m2)

  implicit none
  real (kind=prec) ss,m1,m2,sq_lambda

  sq_lambda = (ss - (m1+m2)**2)*(ss - (m1-m2)**2)
  sq_lambda = sqrt(sq_lambda)

  END FUNCTION SQ_LAMBDA

  SUBROUTINE PAIR_DEC(random_array,min,q3,m3,q4,m4,enough_energy)

         !!  q3^3 = m3^2;  q4^2 = m4^2;  (q3+q4)^2 = min^2     !!

  real (kind=prec), intent(in) :: random_array(2),min,m3,m4
  real (kind=prec), intent(out) :: q3(4),q4(4)
  integer, intent(out) :: enough_energy
  real (kind=prec) :: pp, e3, e4,                               &
            phi3, sin_th3, cos_th3, sin_phi3,  cos_phi3

  if(min > m3+m4) then
    enough_energy = 1
  else
    enough_energy = 0
    q3 = 0._prec; q4 = 0._prec;
    return
  endif

         ! Generate q3 and q4 in rest frame of q3+q4

  e3 = 0.5*(min+(m3**2-m4**2)/min)
  e4 = 0.5*(min+(m4**2-m3**2)/min)
  pp = 0.5*sq_lambda(min**2,m3,m4)/min

  phi3 = 2*pi*random_array(1)
  cos_th3 = 2*random_array(2) - 1._prec
  sin_th3 = sqrt(1 - cos_th3**2)
  sin_phi3 = sin(phi3)
  cos_phi3 = cos(phi3)

  q3 = (/ pp*sin_th3*cos_phi3, pp*sin_th3*sin_phi3, pp*cos_th3, e3/)
  q4 = (/ -pp*sin_th3*cos_phi3, -pp*sin_th3*sin_phi3, -pp*cos_th3, e4/)

  END SUBROUTINE PAIR_DEC

  SUBROUTINE PSX2(ra,p1,m1,p2,m2,q1,m3,q2,m4,weight)

       !!                       in  c.m.f.                    !!
       !!                   p1 + p2 = q1 + q2                 !!
       !!  q1^2 = M1^2;  q2^2 = m2^2; q3^2 = m3^2 ; q4^2 = m4^2  !!

  real (kind=prec), intent(in) :: ra(2), m1, m2, m3, m4
  real (kind=prec), intent(out) :: weight, p1(4), p2(4), q1(4), q2(4)
  integer :: enough_energy
  real (kind=prec) :: e1,e2

  e1 = (scms + m1**2 - m2**2) / 2 / sqrt(scms)
  e2 = sqrt(scms) - e1

  p1 = (/ 0._prec, 0._prec, sqrt(e1**2-m1**2) ,e1/)
  p2 = (/ 0._prec, 0._prec,-sqrt(e2**2-m2**2) ,e2/)

  !TODO change factor 4 in mat_el of ee2nn
  weight = 1._prec
  call pair_dec(ra(1:2),sqrt(scms), q1, m3, q2, m4,enough_energy)

  if(enough_energy == 0) goto 999
  weight = weight*0.125*sq_lambda(scms,m3,m4)/scms/pi


  return
999 continue
  p1 = 0.
  p2 = 0.
  q1 = 0.
  q2 = 0.
  weight = 0.
  return
  END SUBROUTINE PSX2


                          !!!!!!!!!!!!!!!!!!!!!!
                           END MODULE FUNCTIONS
                          !!!!!!!!!!!!!!!!!!!!!!
