  PROGRAM MCMULE
  use vegas_m

  implicit none

  integer :: ndim
  integer :: ran_seed
  integer :: ncall_ad, itmx_ad, ncall, itmx, nenter_ad, nenter
  integer :: initial_ran_seed, ad_print, fu_print
  character (len=1) ::  ad_char, fu_char
  character (len=10) :: which_piece
  real (kind=prec) :: avgi, sd, chi2a

  read(5,*) nenter_ad
  read(5,*) itmx_ad
  read(5,*) nenter
  read(5,*) itmx
  read(5,*) ran_seed
  read(5,*) scms
  read(5,*) which_piece

  initial_ran_seed = ran_seed
  ncall_ad = 1000*nenter_ad
  ncall = 1000*nenter


      !!!!!!!!!!      OUTPUT     !!!!!!!!!

  ad_print = ncall_ad
  ad_char = ' '
  if(ncall_ad > 999999999) then
    ad_print = ncall_ad/1000000000
    ad_char = 'G'
  elseif(ncall_ad > 999999) then
    ad_print = ncall_ad/1000000
    ad_char = 'M'
  elseif(ncall_ad > 999) then
    ad_print = ncall_ad/1000
    ad_char = 'K'
  endif

  fu_print = ncall
  fu_char = ' '
  if(ncall > 999999999) then
    fu_print = ncall/1000000000
    fu_char = 'G'
  elseif(ncall > 999999) then
    fu_print = ncall/1000000
    fu_char = 'M'
  elseif(ncall > 999) then
    fu_print = ncall/1000
    fu_char = 'K'
  endif


  write(6,*) '  - * - * - * - * - * - * - * -     '
  write(6,*) '        '

  select case(which_piece)
    case('em2em0')
      call vegas (2,ncall_ad,itmx_ad,sigma_0_em2em,avgi,sd,chi2a,ran_seed)
      call vegas1(2,ncall   ,itmx   ,sigma_0_em2em,avgi,sd,chi2a,ran_seed)
    case('ee2ee0')
      call vegas (2,ncall_ad,itmx_ad,sigma_0_ee2ee,avgi,sd,chi2a,ran_seed)
      call vegas1(2,ncall   ,itmx   ,sigma_0_ee2ee,avgi,sd,chi2a,ran_seed)
    case default
      stop
  end select


  write(6,"(a,i3,a,i3,a,a,i3,a,i3,a,a,i10)")             &
      ' points:   ',itmx_ad,'*',ad_print,ad_char,' + ',   &
      itmx,'*',fu_print,fu_char,                         &
      '         random seed: ', initial_ran_seed
  write(6,"(a,a)")     &
      ' part: ', which_piece
  write(6,*) '        '
  write(6,*) '        '
  write(6,*) '        '
!  write(6,"(a,es13.5,a,es13.5,a,f4.2)")     &
!      ' result, error:  {' ,conv*avgi ,',', conv*sd,' };   chisq:  ', chi2a
  write(6,"(a,es13.5,a,es13.5,a,f4.2)")     &
      ' result, error:  {' ,avgi ,',', sd,' };   chisq:  ', chi2a
  write(6,*) '        '
  write(6,*) '  - * - * - * - * - * - * - * -     '

contains


  FUNCTION SIGMA_0_EM2EM(x, wgt, ndim)
  use functions
  use mat_el
  use user
  integer :: ndim
  real(kind=prec) :: x(ndim), wgt, sigma_0_em2em
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), weight

  sigma_0_em2em = 0.

  call psx2(x, p1, Mel, p2, Mmu, p3, Mel, p4, Mmu, weight)
  if(weight > 0) then
    if(pass_cut(p1, p2, p3, p4)) then
      sigma_0_em2em = em2em(p1, p2, p3, p4)
      sigma_0_em2em = sigma_0_em2em * weight
    endif
  endif

  END FUNCTION SIGMA_0_EM2EM


  FUNCTION SIGMA_0_EE2EE(x, wgt, ndim)
  use functions
  use mat_el
  use user
  integer :: ndim
  real(kind=prec) :: x(ndim), wgt, sigma_0_ee2ee
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), weight

  sigma_0_ee2ee = 0.

  call psx2(x, p1, Mel, p2, Mel, p3, Mel, p4, Mel, weight)
  if(weight > 0) then
    if(pass_cut(p1, p2, p3, p4)) then
      sigma_0_ee2ee = ee2ee(p1, p2, p3, p4)
      sigma_0_ee2ee = sigma_0_ee2ee * weight
    endif
  endif

  END FUNCTION SIGMA_0_EE2EE

  END PROGRAM MCMULE

