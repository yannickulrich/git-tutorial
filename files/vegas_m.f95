



                 !!!!!!!!!!!!!!!!!!!!!!!
                     MODULE  VEGAS_M
                 !!!!!!!!!!!!!!!!!!!!!!!

  use functions


  integer, parameter :: maxdim = 17
  real(kind=selected_real_kind(15,32)), private:: xi(50,maxdim)
         ! as  this  line is absent in old version

  ! other cumulative variables absent in the old version (this used to be the
  ! common block bveg2
  integer, private :: it, ndo
  real(kind=selected_real_kind(15,32)), private:: si, swgt, schi

  real(kind=prec), private :: starttime
  real(kind=prec) :: alph
  character(len=5) :: oldsha

!==!      common/bveg2/it,ndo,si,swgt,schi,xi(50,maxdim)

  contains


!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Copyright (c) 1976/1988/1995 G.P. Lepage, Cornell University c
! Permission is granted for anyone to use vegas software       c
! for any purpose on any computer. The author is not           c
! responsible for the consequences of such use. The            c
! software is meant to be freely distributed.                  c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!    <<<<<<<<<<< CHANGED VERSION Adrian Signer >>>>>>>>>>>>>

!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! the following routines are essential to vegas()
!
! N.B. Integrals up to dimension "maxdim" can be done with the code
!     currently maxdim = 17 :: if maxdim is changed
!     explicit change  x(17) -> x(??) needed in line 64



      subroutine vegas(ndim,ncall,itmx,fxn,avgi,sd,chi2a,randy)
!
!   ndim-dimensional Monte Carlo integration of fxn
!        - gp lepage  sept 1976/(rev)aug 1979  (j comp phys 27,192(1978))
!
!   arguments:
!       ndim = no of dimensions
!       fxn  = integrand f(x,wgt) where x(i)=integ'n pt (can ignore wgt)
!       avgi = Monte Carlo estimate of integral
!       sd   = error estimate for answer
!       chi2a= chi^2/dof test that different iterations agree
!              (should be of order 1 or less)
!
!   implicit arguments (via common block; all have default values):
!       ncall = number of fxn evaluations per iteration
!       itmx  = number of iterations
!       xl(i) = lower limit of integration in direction i
!       xu(i) = upper limit of integration in direction i
!       acc   = accuracy desired
!
      implicit real(kind=selected_real_kind(15,32))(a-h,o-z)
      logical resume
!==!      common/bveg1/ndev,xl(maxdim),xu(maxdim),acc
!==!      common/bveg2/it,ndo,si,swgt,schi,xi(50,maxdim)
!==!      common/bveg3/alph,ndmx,mds
      dimension d(50,maxdim),di(50,maxdim),xin(50),r(50),dx(maxdim), &
                ia(maxdim),kg(maxdim),dt(maxdim),                    &
                x(maxdim),xl(maxdim),xu(maxdim) !! as see fist line !! ,xi(50,maxdim)
      integer randy
!      real ran2
      real(kind=selected_real_kind(15,32)) smth
      interface
        function fxn(x,wgt,ndim)
          integer :: ndim
          real(kind=selected_real_kind(15,32)) :: x(ndim),wgt,fxn
                                               !! x(maxdim)
        end function fxn
      end interface
!      external fxn
      data smth/2/

!  initiallize non needed random variables to any value

!      do i= ndim+1,12
!         x(i) = 0.3d0
!      enddo

!
!  I've put the default assignments which will NOT be overridden here
!
      do i = 1,maxdim
        xl(i) = 0.d0
        xu(i) = 1.d0
        do ii = 1,50
          xi(ii,i) = 1.d0
        enddo
      enddo

      alph = 1.5d0
      ndmx = 50
      mds = 1
      ndev = 6
      ndo = 1
      it = 0
      si = 0.d0
      swgt = 0.d0
      schi = 0.d0
      acc = -1.d0
!
!     end default assignments
!
      ndo=1
      do j=1,ndim
        xi(1,j)=1.d0
      enddo
!
      entry vegas1(ndim,ncall,itmx,fxn,avgi,sd,chi2a,randy)
      call cpu_time(starttime)
!         - initializes cummulative variables, but not the grid
      it=0
      si=0.d0
      swgt=si
      schi=si
!
      entry vegas2(ndim,ncall,itmx,fxn,avgi,sd,chi2a,randy)
!         - no initialization
998   nd=ndmx
      ng=1
      if(mds.eq.0) go to 2
      ng=(ncall/2.)**(1./ndim)
      mds=1
      if((2*ng-ndmx).lt.0) go to 2
      mds=-1
      npg=ng/ndmx+1
      nd=ng/npg
      ng=npg*nd
 2    k=ng**ndim
      npg=ncall/k
      if(npg.lt.2) npg=2
      calls=npg*k
      dxg=1d0/ng
      dv2g=(calls*dxg**ndim)**2/npg/npg/(npg-1d0)
      xnd=nd
      ndm=nd-1
      dxg=dxg*xnd
      xjac=1./calls
      do j=1,ndim
        dx(j)=xu(j)-xl(j)
        xjac=xjac*dx(j)
      enddo
!
!   rebin, preserving densities
      if(nd.eq.ndo) go to 8
      rc=ndo/xnd
      do  j=1,ndim
        k=0
        xn=0
        dr=xn
        i=k
 4      k=k+1
        dr=dr+1.
        xo=xn
        xn=xi(k,j)
 5      if(rc.gt.dr) go to 4
        i=i+1
        dr=dr-rc
        xin(i)=xn-(xn-xo)*dr
        if(i.lt.ndm) go to 5
        do  i=1,ndm
         xi(i,j)=xin(i)
        enddo
        xi(nd,j)=1.d0
      enddo
      ndo=nd
!
 8    continue
! if(nprn.ge.0) write(ndev,200) ndim,calls,it,itmx,acc,nprn, &
!                          alph,mds,nd,(j,xl(j),j,xu(j),j=1,ndim)
!
      entry vegas3(ndim,fxn,avgi,sd,chi2a)
!         - main integration loop
 9    it=it+1
      ti=0.
      tsi=ti
      do  j=1,ndim
        kg(j)=1
        do  i=1,nd
          d(i,j)=ti
          di(i,j)=ti
        enddo
      enddo

 11   fb=0.
      f2b=fb
      k=0
 12   k=k+1
      wgt=xjac
      do  j=1,ndim
        xn=(kg(j)-ran2(randy))*dxg+1.
        ia(j)=xn
        if(ia(j).gt.1) go to 13
        xo=xi(ia(j),j)
        rc=(xn-ia(j))*xo
        go to 14
 13     xo=xi(ia(j),j)-xi(ia(j)-1,j)
        rc=xi(ia(j)-1,j)+(xn-ia(j))*xo
 14     x(j)=xl(j)+rc*dx(j)
      wgt=wgt*xo*xnd
      enddo
!
      f=wgt
      f=f*fxn(x,wgt,ndim)
      f2=f*f
      fb=fb+f
      f2b=f2b+f2
      do  j=1,ndim
        di(ia(j),j)=di(ia(j),j)+f
        if(mds.ge.0) d(ia(j),j)=d(ia(j),j)+f2
      enddo
      if(k.lt.npg) go to 12
!
      f2b=sqrt(f2b*npg)
      f2b=(f2b-fb)*(f2b+fb)
      ti=ti+fb
      tsi=tsi+f2b
      if(mds.ge.0) go to 18
      do  j=1,ndim
        d(ia(j),j)=d(ia(j),j)+f2b
      enddo
 18   k=ndim
 19   kg(k)=mod(kg(k),ng)+1
      if(kg(k).ne.1) go to 11
      k=k-1
      if(k.gt.0) go to 19
!
!   compute final results for this iteration
      tsi=tsi*dv2g
      ti2=ti*ti
      wgt=1./tsi
      si=si+ti*wgt
      swgt=swgt+wgt
      schi=schi+ti2*wgt
      avgi=si/swgt
      chi2a=(schi-si*avgi)/(it-.9999)
      sd=sqrt(1./swgt)
      tsi=sqrt(tsi)
      print*,"internal avgi, sd: ", avgi, sd,chi2a  !control!
!
!
!   refine grid
 21   do  j=1,ndim
        xo=d(1,j)
        xn=d(2,j)
        d(1,j)=(smth*xo+xn)/(smth+1)
        dt(j)=d(1,j)
        do  i=2,ndm
          d(i,j)=xo+smth*xn
          xo=xn
          xn=d(i+1,j)
          d(i,j)=(d(i,j)+xn)/(smth+2)
          dt(j)=dt(j)+d(i,j)
        enddo
      d(nd,j)=(xo+smth*xn)/(smth+1)
        dt(j)=dt(j)+d(nd,j)
      enddo
!
      do  j=1,ndim
      rc=0.
      do  i=1,nd
        r(i)=0.
        if(d(i,j).le.0.) go to 24
        xo=dt(j)/d(i,j)
        r(i)=((xo-1.)/xo/log(xo))**alph
 24     continue
        rc=rc+r(i)
      enddo
      rc=rc/xnd
      k=0
      xn=0.
      dr=xn
      i=k
 25   k=k+1
      dr=dr+r(k)
      xo=xn
      xn=xi(k,j)
 26   if(rc.gt.dr) go to 25
      i=i+1
      dr=dr-rc
      xin(i)=xn-(xn-xo)*dr/r(k)
      if(i.lt.ndm) go to 26
        do  i=1,ndm
          xi(i,j)=xin(i)
        enddo
        xi(nd,j)=1.
      enddo

!
      if(it.lt.itmx.and.acc*abs(avgi).lt.sd) go to 9
!
999   return
      end subroutine vegas


      FUNCTION RAN2(randy)

            ! This is the usual "random"

      implicit none
      real(kind=prec) :: MINV,RAN2
      integer m,a,Qran,r,hi,lo,randy
      PARAMETER(M=2147483647,A=16807,Qran=127773,R=2836)
      PARAMETER(MINV=0.46566128752458e-09_prec)
      HI = RANDY/Qran
      LO = MOD(RANDY,Qran)
      RANDY = A*LO - R*HI
      IF(RANDY.LE.0) RANDY = RANDY + M
      RAN2 = RANDY*MINV
      END FUNCTION RAN2


                 !!!!!!!!!!!!!!!!!!!!!!!!!!
                     END MODULE  VEGAS_M
                 !!!!!!!!!!!!!!!!!!!!!!!!!!

