                 !!!!!!!!!!!!!!!!!!!!!!!!!
                       MODULE MAT_EL
                 !!!!!!!!!!!!!!!!!!!!!!!!!
  use functions

contains

  FUNCTION EM2EM(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive (and massless) electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: ss,tt, me2,mm2
  real (kind=prec) :: em2em

  ss = sq(p1+p2); tt = sq(p1-q1)
  me2=sq(p1); mm2=sq(p2)

  em2em = (2*(me2 + mm2 - ss)**2 + 2*ss*tt + tt**2)/(tt**2)

  em2em = 32*pi**2*alpha**2*em2em

  END FUNCTION EM2EM

  FUNCTION EE2EE_TT(p1,p2,q1,q2)
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: ss,tt, m2
  real (kind=prec) :: ee2ee_tt

  ss = sq(p1+p2)
  tt = sq(p1-q1)
  m2=sq(p1)

  ee2ee_tt = (2*(-2*m2 + ss)**2 + 2*ss*tt + tt**2)/tt**2
  ee2ee_tt = 32 * pi**2 * alpha**2 * ee2ee_tt
  END FUNCTION EE2EE_TT

  FUNCTION EE2EE_TU(p1,p2,q1,q2)
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: ss,tt, m2
  real (kind=prec) :: ee2ee_tu

  ss = sq(p1+p2)
  tt = sq(p1-q1)
  m2=sq(p1)

  ee2ee_tu = -((12*m2**2 - 8*m2*ss + ss**2)/(tt*(-4*m2 + ss + tt)))
  ee2ee_tu = 32 * pi**2 * alpha**2 * ee2ee_tu
  END FUNCTION EE2EE_TU

  FUNCTION EE2EE(p1,p2,q1,q2)
    !! e-(p1) e-(p2) -> e-(q1) e-(q2)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: ee2ee

  ee2ee = ee2ee_tt(p1,p2,q1,q2) + ee2ee_tu(p1,p2,q1,q2)&
        + ee2ee_tt(p1,p2,q2,q1) + ee2ee_tu(p1,p2,q2,q1)

  END FUNCTION EE2EE


  FUNCTION EB2EB(p1,p2,q1,q2)
    !! e-(p1) e+(p2) -> e-(q1) e+(q2)
    !! for massive electrons & positrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: eb2eb

  eb2eb = ee2ee(p1,-q2,q1,-p2)
  END FUNCTION EB2EB
                 !!!!!!!!!!!!!!!!!!!!!!!!!
                     END MODULE MAT_EL
                 !!!!!!!!!!!!!!!!!!!!!!!!!
