
                 !!!!!!!!!!!!!!!!!!!!!
                     MODULE  USER
                 !!!!!!!!!!!!!!!!!!!!!

  use functions

  implicit none


  contains

  FUNCTION PASS_CUT(q1,q2,q3,q4,q5,q6,q7)

  real (kind=prec), intent(in), optional :: q1(4),q2(4),q3(4),q4(4), q5(4),q6(4),q7(4)
  logical ::  pass_cut
  real(kind=prec) :: t13

  pass_cut = .true.

  t13 = sq(q1-q3)
  if (t13 > -10000) pass_cut = .false.
  if (t13 < -30000) pass_cut = .false.

  END FUNCTION PASS_CUT


                 !!!!!!!!!!!!!!!!!!!!!!!
                     END MODULE  USER
                 !!!!!!!!!!!!!!!!!!!!!!!



