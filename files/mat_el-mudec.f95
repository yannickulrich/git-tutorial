                 !!!!!!!!!!!!!!!!!!!!!!!!!
                       MODULE MAT_EL
                 !!!!!!!!!!!!!!!!!!!!!!!!!
  use functions

contains

  FUNCTION EM2EM(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive (and massless) electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: ss,tt, me2,mm2
  real (kind=prec) :: em2em

  ss = sq(p1+p2); tt = sq(p1-q1)
  me2=sq(p1); mm2=sq(p2)

  em2em = (2*(me2 + mm2 - ss)**2 + 2*ss*tt + tt**2)/(tt**2)

  em2em = 32*pi**2*alpha**2*em2em

  END FUNCTION EM2EM


  FUNCTION M2ENN(q1,q2,q3,q4)

  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4)
  real (kind=prec) :: s12,s13,s14,s23,s24,s34
  real (kind=prec) :: m2enn

  s12 = s(q1,q2); s13 = s(q1,q3); s14 = s(q1,q4);
  s23 = s(q2,q3); s24 = s(q2,q4);
  s34 = s(q3,q4);

  m2enn = 4.*s13*s24

  m2enn = 8.*GF**2*m2enn

  m2enn = 0.5*m2enn  ! spin average

  END FUNCTION M2ENN



  FUNCTION M2ENNav(q1,q2, q3, q4, lin)
  ! average over neutrino tensor taken
  real (kind=prec) :: q1(4),q2(4), q3(4), q4(4)
  real (kind=prec), optional :: lin
  real (kind=prec) :: s12
  real (kind=prec) :: m2ennav, Mm2, Me2

   !call crash("m2ennav")

  s12 = s(q1,q2);
  Mm2 = sq(q1)
  Me2 = sq(q2)

  m2ennav = (-8*Me2*Mm2)/3. + 2*Me2*s12 + 2*Mm2*s12 - (4*s12**2)/3.

  m2ennav = 8.*GF**2*m2ennav

  m2ennav = 0.5*m2ennav  ! spin average

  if (present(lin)) then
    ! d-dim neutrino
    lin = (8*me2*Mm2)/9. - 2*me2*s12 - 2*Mm2*s12 + (16*s12**2)/9.
    lin = 8.*GF**2*lin

    lin = 0.5*lin  ! spin average

  endif

  END FUNCTION M2ENNav



  FUNCTION PM2EJ(p1,n1,p2,p3)
  !! mu+(p1) -> e+(p2) J(p3)
  !! mu-(p1) -> e-(p2) J(p3)

  real (kind=prec) :: p1(4), n1(4), p2(4), p3(4)
  real (kind=prec) :: Mmu, Mel, Mj, Mmu2, Mel2, Mj2, nq
  real (kind=prec) :: pm2ej

  Mmu2 = sq(p1)
  Mel2 = sq(p2)
  Mj2 = sq(p3)
  Mmu = sqrt(Mmu2)
  Mel = sqrt(Mel2)
  Mj = sqrt(Mj2)

  nq = s(n1,p2)

  pm2ej = CRj*(2*CLj*Mel*Mmu + CRj*(Mel2 - Mj2 + Mmu2 - Mmu*nq)) &
    + CLj*(2*CRj*Mel*Mmu + CLj*(Mel2 - Mj2 + Mmu2 + Mmu*nq))

  END FUNCTION PM2EJ

                 !!!!!!!!!!!!!!!!!!!!!!!!!
                     END MODULE MAT_EL
                 !!!!!!!!!!!!!!!!!!!!!!!!!
