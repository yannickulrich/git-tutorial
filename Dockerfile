FROM alpine:3.12
RUN apk add coreutils git bash
RUN echo "touch main && chmod +x main ; true" > /usr/bin/make && chmod +x /usr/bin/make
RUN git config --global user.email "marc@dur.ac.uk" && \
    git config --global user.name "Marc"
#COPY . /git
#WORKDIR /git
#RUN INTERACTIVE=0 ./build-project.sh
